"""Tests API functionality."""
import unittest
from unittest import mock
import queue
from gossip.listeners import api


class TestAPIListener(unittest.TestCase):
    """Tests regarding the API listener."""

    def setUp(self):
        """Prepare some variables before each test."""
        self.conn = 'mocked conn'
        self.sock_addr = 'mocked sock_addr'
        self.queue = queue.Queue()

    def test_thread_finish(self):
        """Check if finish flag works well on thread."""
        with mock.patch('socket.socket') as mock_socket:
            mock_socket.accept = mock.Mock(return_value=[self.conn,
                                                         self.sock_addr])
            target = api.APIListener(sock=mock_socket, queue=self.queue)
            target.finish = True
            target.start()
            target.join(timeout=1)
            assert(not target.is_alive())

    def test_data_conn_queue(self):
        """Check if data sent is actually received."""
        with mock.patch('socket.socket') as mock_socket:
            # mock accept aswell
            mock_socket.accept = mock.Mock(return_value=[self.conn,
                                                         self.sock_addr])
            target = api.APIListener(sock=mock_socket, queue=self.queue)
            target.start()
            # Wait until APIListener does its job
            while self.queue.empty():
                pass
            target.finish = True
            target.join()

            val = self.queue.get()
            assert(val['type'] == api.APIListener)
            assert(val['conn'] == self.conn)

    def test_data_recv_exception(self):
        """Force an OSError exception."""
        with mock.patch('socket.socket') as mock_socket:
            mock_socket.accept.side_effect = OSError(mock.Mock(status=123),
                                                     'mocked up error')
            target = api.APIListener(sock=mock_socket, queue=self.queue)
            target.start()
            target.join(timeout=1)
            assert(not target.is_alive())
