"""Tests for Handler skeleton."""
import unittest
from gossip.handlers.api.Handler import Handler


class TestSkelHandler(unittest.TestCase):
    """Tests regarding main."""

    def setUp(self):
        """Setup test data and test target."""
        self.params = dict()
        self.target = Handler(params=self.params)

    def test_vars(self):
        """Test for variables being set."""
        self.assertEqual(self.target.params, self.params)

    def test_parse_exception(self):
        """Test that methdos return exceptions."""
        self.assertRaises(NotImplementedError, self.target.parse)

    def test_action_exception(self):
        """Test that methdos return exceptions."""
        self.assertRaises(NotImplementedError, self.target.action)
