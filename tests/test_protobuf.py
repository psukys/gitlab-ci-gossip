"""Test/Showcase protobuffer functionality."""
import unittest
from gossip.p2p.p2p_pb2 import MessageWrapper


class TestProtobuf(unittest.TestCase):
    """Tests regarding protobuf."""

    def test_hello_auth(self):
        """Test HelloAuth message type."""
        msg = MessageWrapper()
        msg.type = MessageWrapper.HELLOAUTH
        msg.helloAuth.gossip_const = 1337
        msg.helloAuth.id = 42

        # msg is initialized and only helloAuth is set.
        self.assertTrue(msg.IsInitialized)
        self.assertTrue(msg.HasField("helloAuth"))
        self.assertFalse(msg.HasField("idSpread"))
        self.assertFalse(msg.HasField("dataSpread"))

    def test_id_spread(self):
        """Test IdSpread message type and basic usage."""
        msg = MessageWrapper()
        msg.type = MessageWrapper.IDSPREAD
        msg.idSpread.type = msg.idSpread.PUSH_REQUEST
        peer = msg.idSpread.peers.add()
        peer.ip = "123.123.123.123"
        peer.id = 42

        # msg is initialized and only idSpread is set.
        self.assertTrue(msg.IsInitialized)
        self.assertFalse(msg.HasField("helloAuth"))
        self.assertTrue(msg.HasField("idSpread"))
        self.assertFalse(msg.HasField("dataSpread"))

        # Serialize the data
        data = msg.SerializeToString()

        # Parse serialized data (e.g. from a socket)
        new_msg = MessageWrapper()
        new_msg.ParseFromString(data)

        self.assertEqual(new_msg.type, MessageWrapper.IDSPREAD)
        self.assertEqual(len(new_msg.idSpread.peers), 1)
        self.assertEqual(new_msg.idSpread.peers[0].id, 42)
        self.assertEqual(new_msg.idSpread.peers[0].ip, "123.123.123.123")

    def test_data_spread(self):
        """Test DataSpread message type."""
        msg = MessageWrapper()
        msg.type = MessageWrapper.DATASPREAD
        msg.dataSpread.ttl = 64
        msg.dataSpread.data_type = 1337
        msg.dataSpread.data = bytes()

        # msg is initialized and only dataSpread is set.
        self.assertTrue(msg.IsInitialized)
        self.assertFalse(msg.HasField("helloAuth"))
        self.assertFalse(msg.HasField("idSpread"))
        self.assertTrue(msg.HasField("dataSpread"))
