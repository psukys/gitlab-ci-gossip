"""Tests that verify correct installed environment."""
import unittest


class TestModuleInstalled(unittest.TestCase):
    """Tests regarding the Python module integrity."""

    def test_import(self):
        """Smoke test on importing gossip module."""
        try:
            import gossip
        except ImportError:
            self.fail('Module failed to install')
