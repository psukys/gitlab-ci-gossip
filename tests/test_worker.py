"""Tests worker functionality."""
import unittest
from unittest import mock
import queue
from gossip.Worker import Worker
from gossip.listeners.api import APIListener
from gossip.listeners.api import APIListener
from gossip.util.Subscriptions import Subscriptions
from gossip.util.notifications import Notifications


class TestWorker(unittest.TestCase):
    """Tests regarding the Worker thread."""

    def setUp(self):
        """Prepare some variables before each test."""
        self.work_queue = queue.Queue()
        self.p2p_queue = queue.Queue()
        self.api_queue = queue.Queue()
        self.subscriptions = Subscriptions()
        self.notifications = Notifications(10)
        self.mock_socket = mock.patch('socket.socket')
        self.mock_socket.recv = lambda x: ''
        self.target = Worker(work_queue=self.work_queue,
                             p2p_output=self.p2p_queue,
                             api_output=self.api_queue,
                             subscriptions=self.subscriptions,
                             notifications=self.notifications)

    def tearDown(self):
        """Cleanup after the test."""
        # tell thread to finish
        # https://stackoverflow.com/a/323993/552214
        self.target.finish = True
        # the worker stops at waiting for queue
        # simulate new item
        self.work_queue.put({'conn': self.mock_socket, 'type': APIListener})

    def test_thread_finish(self):
        """Check if finish flag works well on thread."""
        self.target.finish = True
        self.target.start()
        self.target.join(timeout=1)
        assert(not self.target.is_alive())

    def test_socket_read_smaller_than_block(self):
        """Check socket read functionality on data smaller than block."""
        data = ['1']
        expected = ''.join(data)
        block_size = 2
        self.mock_socket.recv = lambda x: data.pop(0) if data else ''
        res = self.target.read_socket_data(sock=self.mock_socket,
                                           block=block_size)
        self.assertEqual(res, expected)

    def test_socket_read_equal_to_block(self):
        """Check socket read functionality on data equal to block."""
        data = ['1', '2', '3', '4', '5']
        expected = ''.join(data)
        block_size = 1  # data length is only 1
        self.mock_socket.recv = lambda x: data.pop(0) if data else ''
        res = self.target.read_socket_data(sock=self.mock_socket,
                                           block=block_size)
        self.assertEqual(res, expected)

    def test_socket_read_bigger_than_block(self):
        """Check socket read functionality on data bigger than block."""
        data = ['123', '321', '456', '23', '199']
        expected = ''.join(data)
        block_size = 2
        self.mock_socket.recv = lambda x: data.pop(0) if data else ''
        res = self.target.read_socket_data(sock=self.mock_socket,
                                           block=block_size)
        self.assertEqual(res, expected)

    def test_socket_read_oserror(self):
        """Check socket read functionality on data bigger than block."""
        self.mock_socket.recv.side_effect = OSError(mock.Mock(status=123),
                                                    'mocked up error')
        res = self.target.read_socket_data(sock=self.mock_socket)
        self.assertEqual(res, '')

    def test_bad_api_message_handle_doesnt_stop(self):
        """Check whether the worker still goes regardless of bad api handle."""
        data = ['1', '2', '3', '4', '5']
        self.mock_socket.recv = lambda x: data.pop(0) if data else ''
        self.work_queue.put({'type': APIListener,
                             'conn': self.mock_socket})
        self.target.start()
        self.target.join(timeout=2)  # better solution?
        self.assertTrue(self.target.is_alive())
