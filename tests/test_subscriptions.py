"""Tests thread safe dict functionality."""
import unittest
import threading
import socket
from gossip.util.Subscriptions import Subscription, Subscriptions


class TestSubscription(unittest.TestCase):
    """Test for thread safe implementation of subscription."""

    def setUp(self):
        """Setup target."""
        self.target = Subscription()

    def test_isarray(self):
        """Test subscription is an array."""
        self.assertTrue(isinstance(self.target, list))

    def test_init_has_lock(self):
        """Test setup creates a lock."""
        self.assertFalse(self.target.lock is None)

    def test_append(self):
        """Test that append does not happen if locked."""
        def threaded_append(target):
            target.append(1)

        self.target.lock.acquire()
        thread = threading.Thread(target=threaded_append, args=(self.target,))
        thread.start()
        self.assertEqual(len(self.target), 0)
        self.target.lock.release()
        thread.join()

    def test_setitem(self):
        """Test that setitem does not happen if locked."""
        def threaded_setitem(target):
            target[0] = 123

        test_value = 0
        self.target.append(test_value)

        self.target.lock.acquire()

        thread = threading.Thread(target=threaded_setitem, args=(self.target,))
        thread.start()

        self.assertEqual(len(self.target), 1)

        self.target.lock.release()
        thread.join()

    def test_getitem(self):
        """Test that getitem does not happen if locked."""
        def threaded_getitem(target):
            return target[0]

        test_value = 0
        self.target.append(test_value)

        self.target.lock.acquire()
        thread = threading.Thread(target=threaded_getitem, args=(self.target,))
        thread.start()
        thread.join(timeout=1)

        self.assertTrue(thread.is_alive())
        self.target.lock.release()
        thread.join()

    def test_remove(self):
        """Test for removing functionality."""
        with unittest.mock.patch('socket.socket') as mock_socket:
            self.target.append(mock_socket)
            self.assertEqual(len(self.target), 1)
            self.target.remove(mock_socket)
            self.assertEqual(len(self.target), 0)

    def test_remove_invalid(self):
        """Test for removing, when diff socket is specified."""
        s1 = socket.socket()
        s2 = socket.socket()
        self.target.append(s1)
        self.assertEqual(len(self.target), 1)
        self.assertRaises(ValueError, self.target.remove, s2)
        s1.close()
        s2.close()


class TestSubscriptions(unittest.TestCase):
    """Test for thread safe implementation of subscriptions."""

    def setUp(self):
        """Setup target."""
        self.target = Subscriptions()

    def test_isdict(self):
        """Test subscription is an array."""
        self.assertTrue(isinstance(self.target, dict))

    def test_init_has_lock(self):
        """Test setup creates a lock."""
        self.assertFalse(self.target.lock is None)

    def test_setitem_new_locked(self):
        """Test that setitem does not happen if locked."""
        def threaded_setitem(target):
            target['b'] = 123

        test_value = 0
        self.target['a'] = 0

        self.target.lock.acquire()

        thread = threading.Thread(target=threaded_setitem, args=(self.target,))
        thread.start()
        thread.join(timeout=1)
        # thread shouldn't finish - if not, lock is not used for new
        self.assertTrue(thread.is_alive())

        self.target.lock.release()
        thread.join()

    def test_setitem_same_locked(self):
        """Test that setitem does not happen if locked."""
        def threaded_setitem(target):
            target['a'] = 123

        self.target['a'] = 0

        self.target.lock.acquire()

        thread = threading.Thread(target=threaded_setitem, args=(self.target,))
        thread.start()
        thread.join(timeout=0.5)
        # thread shouldn't finish - if not, lock is not used for new
        self.assertTrue(thread.is_alive())
        self.assertEqual(self.target['a'], 0)

        self.target.lock.release()
        thread.join(timeout=0.5)
        self.assertFalse(thread.is_alive())
        self.assertEqual(self.target['a'], 123)

    def test_delitem_same_locked(self):
        """Test that delitem does not happen if locked."""
        def threaded_setitem(target):
            del target['a']

        self.target['a'] = 0

        self.target.lock.acquire()

        thread = threading.Thread(target=threaded_setitem, args=(self.target,))
        thread.start()
        thread.join(timeout=0.5)
        # thread shouldn't finish - if not, lock is not used for new
        self.assertTrue(thread.is_alive())
        self.assertEqual(self.target['a'], 0)

        self.target.lock.release()
        thread.join(timeout=0.5)
        self.assertFalse(thread.is_alive())
        self.assertFalse('a' in self.target)


class TestSubscriptionsIntegrated(unittest.TestCase):
    """Integrated test Subscription in Subscriptions."""

    def setUp(self):
        """Setup target."""
        self.target = Subscriptions()

    def test_assign(self):
        """Check basic functionality for assigning."""
        data_type = 123
        subscribers = [1, 2, 3, 4, 5]
        self.target[data_type] = Subscription(subscribers)
        self.assertTrue(data_type in self.target)
        self.assertEqual(self.target[data_type], subscribers)

    def test_remove(self):
        """Check for modification functionality."""
        data_type = 123
        subscribers = [1, 2, 3, 4, 5]
        self.target[data_type] = Subscription(subscribers)
        self.assertRaises(ValueError, self.target[data_type].remove, 6)

    def test_conc_append(self):
        """Check concurrent appending."""
        def conc_sub_thread(subs):
            subs[123].append(4)

        def conc_subs_thread(subs):
            subs[999] = Subscription()

        self.target[123] = Subscription([1, 2, 3])

        sub_thread = threading.Thread(target=conc_sub_thread,
                                      args=(self.target,))

        subs_thread = threading.Thread(target=conc_subs_thread,
                                       args=(self.target,))

        sub_thread.start()
        subs_thread.start()

        sub_thread.join()
        subs_thread.join()

        self.assertTrue(999 in self.target)
        self.assertTrue(4 in self.target[123])

    def test_setdefault_thread(self):
        """Test setdefault thread safety."""
        def setdefault_thread(target):
            target.setdefault(123, 123)

        thread = threading.Thread(target=setdefault_thread,
                                  args=(self.target,))
        self.target.lock.acquire()
        thread.start()
        thread.join(0.5)
        self.assertTrue(thread.is_alive())
        self.assertFalse(123 in self.target)
        self.target.lock.release()
        thread.join()
        self.assertEquals(self.target[123], 123)
