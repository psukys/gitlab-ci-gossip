"""Tests outputter functionality."""
import unittest
import time
from unittest import mock
import queue
from gossip.outputters.Outputter import Outputter


class TestOutputter(unittest.TestCase):
    """Tests regarding the  listener."""

    def create_mock_socket(self):
        """Helper function for quick mocking socket."""
        socket_mock = mock.Mock()
        socket_mock.return_value.sendall = mock.Mock()
        return socket_mock

    def queue_empty_timeout(self, timeout: int, time_step: int=1):
        """Helper function to implement timeout check for empty queue."""
        time_elapsed = 0
        while not self.queue.empty() and time_elapsed < timeout:
            time_elapsed += time_step
            time.sleep(time_step)
        return self.queue.empty()

    def setUp(self):
        """Prepare some variables before each test."""
        self.queue = queue.Queue()
        self.target = Outputter(queue=self.queue, name='test')

    def tearDown(self):
        """Cleanup after the test."""
        self.target.finish = True
        # the worker stops at waiting for queue
        # simulate new item
        self.queue.put({'sockets': [self.create_mock_socket()],
                        'data': bytes()})

    def test_thread_finish(self):
        """Check if finish flag works well on thread."""
        self.target.finish = True
        self.target.start()
        self.target.join(timeout=1)
        assert(not self.target.is_alive())

    def test_data_send_one_queue(self):
        """Check if data put is sent for one socket."""
        data = bytes(b'hello')
        timeout = 2
        sock_mock = self.create_mock_socket()

        self.queue.put({'sockets': [sock_mock], 'data': data})
        self.target.start()

        if not self.queue_empty_timeout(timeout):
            self.fail('Queue was not empty after {0}s'.format(timeout))

        sock_mock.sendall.assert_called_with(data)

    def test_data_send_many_queue(self):
        """Check if data put is sent to many sockets (list)."""
        data = bytes(b'hello')
        timeout = 2
        mocked_sockets = 5

        # create 5 mock sockets
        sock_mocks = [self.create_mock_socket()
                      for _ in range(mocked_sockets)]

        self.queue.put({'sockets': sock_mocks, 'data': data})
        self.target.start()

        if not self.queue_empty_timeout(timeout):
            self.fail('Queue was not empty after {0}s'.format(timeout))

        for sock_mock in sock_mocks:
            sock_mock.sendall.assert_called_with(data)

    def test_exc_doesnt_stop(self):
        """Test that exception doesn't stop thread."""
        data = bytes(b'hello')
        sock_mock = self.create_mock_socket()
        sock_mock.sendall.side_effect = Exception('some error')
        self.queue.put({'sockets': [sock_mock], 'data': data})

        self.target.start()
        self.target.join(timeout=2)

        self.assertTrue(self.target.is_alive())

    def test_exc_mixed(self):
        """A mix in socket list - some drop exceptions, others are ok."""
        data = bytes(b'hello')
        good_sock_amount = 7
        bad_sock_amount = 9

        good_socks = [self.create_mock_socket()
                      for _ in range(good_sock_amount)]

        bad_socks = [self.create_mock_socket()
                     for _ in range(bad_sock_amount)]

        for i, s in enumerate(bad_socks):
            bad_socks[i].sendall.side_effect = Exception('some error')

        self.queue.put({'sockets': good_socks + bad_socks,
                        'data': data})

        self.target.start()

        self.target.join(timeout=2)
        self.assertTrue(self.target.is_alive())

        for good_sock in good_socks:
            good_sock.sendall.assert_called_with(data)
