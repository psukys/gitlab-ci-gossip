"""Tests for handler wrapper."""
import unittest
from gossip.handlers.api.APIHandlerWrapper import APIHandlerWrapper
from queue import Queue
from unittest import mock
import struct


class TestAPIHandlerWrapper(unittest.TestCase):
    """Tests regarding API handler wrapper."""

    def setUp(self):
        """Setup target."""
        self.params = {'api_output': Queue(),
                       'p2p_output': Queue()}
        self.target = APIHandlerWrapper(params=self.params)
        self.handler_mock = mock.Mock()
        # only initialized, values have to be set up
        self.handler_mock.api_code = 123
        self.handler_mock.return_value.parse = mock.Mock(return_value=None)
        self.handler_mock.return_value.action = mock.Mock(return_value=None)

    def test_register_handlers(self):
        """Test register_handlers function."""
        # currently not really testable as the method for retrieving is unclear
        handlers = self.target.register_handlers()
        self.assertEqual(type(handlers), list)

    def test_get_handler(self):
        """Check whether handler is returned by code."""
        h1 = mock.Mock()
        h1.api_code = 123
        h2 = mock.Mock()
        h2.api_code = 999
        self.target.handlers = [h1, h2]
        handler = self.target.get_handler(h1.api_code)
        self.assertEqual(h1, handler)

    def test_handle_message_invalid(self):
        """Test handle_message with invalid data."""
        data = bytes()
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_bigger_size(self):
        """Test handle_message with wrong size in header."""
        data = struct.pack('!HHHH', 9, 123, 0, 1337)
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_smaller_size(self):
        """Test handle_message with wrong size in header."""
        data = struct.pack('!HHHH', 7, 123, 0, 1337)
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_no_handler(self):
        """Test handle_message with api_code that no handler has."""
        data = struct.pack('!HHHH', 8, 123, 0, 1337)
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_handler_parse_fail(self):
        """Test handle_message with parser that return false."""
        data = struct.pack('!HHHH', 8, 123, 0, 1337)

        self.handler_mock.return_value.parse.return_value = False
        self.handler_mock.return_value.action.return_value = True
        self.target.handlers = [self.handler_mock]
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_handler_action_fail(self):
        """Test handle_message with action that returns false."""
        data = struct.pack('!HHHH', 8, 123, 0, 1337)

        self.handler_mock.return_value.parse.return_value = True
        self.handler_mock.return_value.action.return_value = False
        self.target.handlers = [self.handler_mock]
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_valid(self):
        """Test handle_message with valid data."""
        # Mock a handler

        self.handler_mock.return_value.parse.return_value = True
        self.handler_mock.return_value.action.return_value = True

        data = struct.pack('!HHHH', 8, 123, 0, 1337)
        self.target.handlers = [self.handler_mock]
        res = self.target.handle_message(message=data)
        self.assertTrue(res)
