"""Tests API listener functionality."""
import unittest
from unittest import mock
from gossip.config import conf


class TestGossipParser(unittest.TestCase):
    """Tests regarding the config parser."""

    def test_valid_read(self):
        """Test valid read."""
        config = conf.GossipConfig("tests/test.conf")

        assert(config.cache_size == 23)
        assert(config.max_connections == 13)

        assert(config.bootstrapper_addr == '1.3.5.7')
        assert(config.bootstrapper_port == 2468)

        assert(config.listen_addr == '1.2.3.4')
        assert(config.listen_port == 3000)

        assert(config.api_addr == '5.6.7.8')
        assert(config.api_port == 2000)

        assert(config.num_workers == 999)

    def test_invalid_read(self):
        """Test invalid read."""
        config = conf.GossipConfig("tests/testerror.conf")
        # Expect default values
        assert(config.cache_size == 50)
        assert(config.max_connections == 30)

        assert(config.bootstrapper_addr == 'fulcrum.net.in.tum.de')
        assert(config.bootstrapper_port == 2086)

        assert(config.listen_addr == '127.0.0.1')
        assert(config.listen_port == 2086)

        assert(config.api_addr == '127.0.0.1')
        assert(config.api_port == 7001)

        assert(config.num_workers == 10)
