# -*- coding: utf-8 -*-
"""Setup gossip module."""

from setuptools import setup, find_packages
import os
import distutils.cmd
import distutils.log
import subprocess


def read(fname):
    """
    Read the text file's contents in current directory.

    Args:
        fname - filename of text file
    """
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


class DoxygenCommand(distutils.cmd.Command):
    """A custom command to generate documentation."""

    description = 'generate doxygen documentation'
    user_options = [
        # The format is (long option, short option, description).
        ('config=', None, 'path to doxygen config file'),
    ]

    def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        self.config = 'Doxyfile'

    def finalize_options(self):
        """Post-process options."""
        if self.config:
            assert os.path.exists(self.config), (
                'Doxygen config file %s does not exist.' % self.config)

    def run(self):
        """Run command."""
        command = ['doxygen']
        if self.config:
            command.append(self.config)
            self.announce(
                'running command: %s' % str(command),
                level=distutils.log.INFO)
            subprocess.check_call(command)


class ProtobufCommand(distutils.cmd.Command):
    """A custom command to generate protobuf."""

    description = 'generate protobuf'
    user_options = [
        # The format is (long option, short option, description).
        ('out=', None, 'path to place generated files'),
        ('src=', None, 'path to .proto src'),
    ]

    def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        self.out = '.'
        self.src = 'gossip/p2p/p2p.proto'

    def finalize_options(self):
        """Post-process options."""
        if self.out:
            assert os.path.exists(self.out), (
                'out path %s does not exist.' % self.out)
        if self.src:
            assert os.path.exists(self.src), (
                'src file %s does not exist.' % self.src)

    def run(self):
        """Run command."""
        command = ['protoc']
        if self.out and self.src:
            command.append('--python_out='+self.out)
            command.append(self.src)
            self.announce(
                'running command: %s' % str(command),
                level=distutils.log.INFO)
            subprocess.check_call(command)


setup(
    name='gossip33',
    packages=find_packages(),
    version='0.1',
    license='GPLv3',
    description='TUM P2P course team 33 gossip project',
    long_description=read('README.md'),
    author='Paulius Šukys, Claes Adam Wendelin',
    author_email='paulius.sukys@tum.de, claesadam.wendelin@tum.de',
    url='https://gitlab.lrz.de/P2P-33/gossip',
    install_requires=[
        'doxypypy',
        'protobuf',
        #  'socket' - already in stdlib
    ],
    test_suite='tests',
    classifiers=[
        # via https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Environment :: No Input/Output (Daemon)',
        'Framework :: AsyncIO',
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Topic :: Internet'
    ],
    cmdclass={
        'docs': DoxygenCommand,
        'proto': ProtobufCommand,
    }
)
