r"""Config provider for Gossip.

\package gossip.config
"""
import configparser
import logging


def parse_ip(ip_port: str):
    r"""
    parse ip:port string

    \param ip_port ip:port

    \return return ip and port
    """
    ip, sep, port = ip_port.rpartition(':')
    if not sep:
        raise KeyError('parse_ip')

    return ip, int(port)


class GossipConfig:
    r"""Config reader and provider class.
    Attributes:

    cache_size: number of cached data items
    max_connections: max number of connected peers
    bootstrapper_addr: bootstrap peer ip
    bootstrapper_port: bootstrap peer port
    listen_addr: ip addr peers connect to you with
    listen_port: port peers connect to you with
    api_addr: ip addr local modules connect to you with
    api_port: port local modules connect to you with
    """
    def __init__(self, config_file: str="sample.conf"):
        r"""Initialize config

        \param config_file name of config file
        """
        self.logger = logging.getLogger(self.__class__.__name__)

        default_cs = 50
        default_mc = 30
        default_boot = 'fulcrum.net.in.tum.de:2086'
        default_listen = '127.0.0.1:2086'
        default_api = '127.0.0.1:7001'
        default_num_workers = 10

        cache_size = default_cs
        max_conn = default_mc
        bootstrapper = default_boot
        listen_address = default_listen
        api_address = default_api
        num_workers = default_num_workers

        config = configparser.ConfigParser()
        config.read(config_file)

        try:
            gossip = config['gossip']

            cache_size = gossip.getint('cache_size', default_cs)
            max_conn = gossip.getint('max_connections', default_mc)
            bootstrapper = gossip.get('bootstrapper', default_boot)
            listen_address = gossip.get('listen_address', default_listen)
            api_address = gossip.get('api_address', default_api)
            num_workers = gossip.get('num_workers', default_num_workers)
        except KeyError as err:
            self.logger.error('KeyError: {0}'.format(err))

        self.cache_size = cache_size
        self.max_connections = max_conn
        addr, port = parse_ip(bootstrapper)
        self.bootstrapper_addr = addr
        self.bootstrapper_port = port
        self.listen_addr, self.listen_port = parse_ip(listen_address)
        self.api_addr, self.api_port = parse_ip(api_address)
        self.num_workers = int(num_workers)

    def __str__(self):
        r"""Print class contents"""
        return str(self.__class__) + ": " + str(self.__dict__)
