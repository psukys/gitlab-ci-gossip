"""Base for API handlers."""


class Handler:
    """Skeleton class for handling."""

    # api_code is not set, but has to be in order for the code to work
    def __init__(self, params: dict):
        """Save data locally in the handler."""
        self.params = params
        self.api_code = -1

    def parse(self):
        """Parse data."""
        raise NotImplementedError('Parse function not implemented')

    def action(self):
        """Work on parsed data."""
        raise NotImplementedError('Action function not implemented')
