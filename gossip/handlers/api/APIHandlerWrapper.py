"""API handler wrapper."""
import logging
import inspect  # to insepct module variables
import sys  # to get current module
import struct
from queue import Queue
from gossip.handlers.api.Handler import Handler


class APIHandlerWrapper:
    """Wrapper class that does deduction on which handler to choose."""

    def __init__(self, params: dict):
        """Setup API wrapper handler."""
        self.logger = logging.getLogger(self.__class__.__name__)
        self.params = params
        self.handlers = self.register_handlers()

    def register_handlers(self) -> list:
        r"""Get a list of handlers in current directory.

        \return         a list of classes that are handlers
        """
        # TODO: make an automatic approach for this
        handlers = []  # class instance should be added
        # i.e. gossip.handlers.api.announce.AnnounceAPI
        return handlers

    def get_handler(self, api_code: int) -> Handler:
        r"""Retrieve specific Handler by its API code.

        \param  api_code    API code (see specification)
        \return             Specific handler (type Handler) or None
        """
        for handler in self.handlers:
            if handler.api_code == api_code:
                return handler

    def handle_message(self, message: bytes) -> bool:
        r"""Handle a raw message.

        \param  message     raw data that likely contains headers and data
        \return             whether handling was successful
        """
        header_size = 4  # set by specification, maybe should be put into conf?
        try:
            size, api_code, data = struct.unpack('!HH{0}s'.format(
                len(message) - header_size), message)
        except struct.error as err:
            self.logger.error(
                'Handle message struct unpack error:\n{0}'.format(err))
            return False

        if size != len(message):
            self.logger.error('Handle message failed: message size ({0})\
             is not as defined in header ({1})'.format(
                                    len(message), size))
            return False

        handler_class = self.get_handler(api_code)
        if not handler_class:
            self.logger.error('No handler classes for {0}\
             api code found'.format(api_code))
            return False
        self.params['data'] = data
        handler = handler_class(self.params)

        parsed = handler.parse()
        if parsed:  # check if message is well-formed
            return handler.action()
        else:
            self.logger.error('Handle message failed due to failed parse.')
            return False
