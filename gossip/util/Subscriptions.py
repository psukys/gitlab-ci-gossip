"""Implementation of a thread safe dictionary."""
import threading
import socket


class Subscription(list):
    """Entry for a subscription dictionary."""

    def __init__(self, *args):
        """Entry for a subscription."""
        list.__init__(self, *args)
        self.lock = threading.Lock()

    def __setitem__(self, key, value):
        """Item setting."""
        try:
            self.lock.acquire()
            list.__setitem__(self, key, value)
        finally:
            # Exception is raised, but release is still issued
            self.lock.release()

    def __getitem__(self, key):
        """Item retrieval."""
        try:
            self.lock.acquire()
            return list.__getitem__(self, key)
        finally:
            self.lock.release()

    def append(self, value):
        """Item appending."""
        try:
            self.lock.acquire()
            list.append(self, value)
        finally:
            # Exception is raised, but release is still issued
            self.lock.release()

    def remove(self, sock: socket.socket):
        r"""Remove item from list.

        \param  sock    socket object to be removed(matches)
        """
        try:
            self.lock.acquire()
            list.remove(self, sock)
        finally:
            self.lock.release()


class Subscriptions(dict):
    """Mechanism for handling subscriptions."""

    def __init__(self, *args):
        """Setup for subscription dict."""
        self.lock = threading.Lock()
        dict.__init__(self, *args)

    def __setitem__(self, key, value: Subscription):
        """Thread safe adding to dict."""
        self.lock.acquire()
        try:
            super(Subscriptions, self).__setitem__(key, value)
        finally:
            self.lock.release()

    def __delitem__(self, key):
        """Thread safe deletion from dict."""
        self.lock.acquire()
        try:
            super(Subscriptions, self).__delitem__(key)
        finally:
            self.lock.release()

    def setdefault(self, key, value):
        """Thread safe setdefault for dict."""
        self.lock.acquire()
        try:
            return super(Subscriptions, self).setdefault(key, value)
        finally:
            self.lock.release()
