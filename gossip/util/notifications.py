"""Thread-safe and size-limited safekeeping of Notifications."""
import threading
from datetime import datetime, timedelta
from collections import OrderedDict

"""
Worker receives Gossip Validation:
if valid: add data to output queue
if !valid: remove notification from dict

This means that the first local module that responds with a Validation
is the one that decide the notification's fate.

Can we trust local modules? Maybe one has better validation than another?
Would have to extend Notification with state that includes
#subscribers to its data_type and #positive validations and
#negative validations received.

If we want it more resistant to misbehaving local modules, we could
consider how many subscribers there are of a certain data_type
and let the majority decide. If equal, positive validation wins.
"""


class Notification:
    r"""Notification class keeping track of state."""

    def __init__(self, data: bytes, ttl: int, timeout_length: int):
        r"""Initialize Notification.

        \param data data encoded in p2p format
        \param ttl time-to-live, 0 infinite
        """
        self.data = data
        self.ttl = ttl
        self.timeout = datetime.now() + timedelta(seconds=timeout_length)

    def is_timed_out(self, curr_time: datetime):
        r"""Check if time is up.

        \param curr_time current time
        """
        return self.timeout <= curr_time


class Notifications(OrderedDict):
    """Thread safe dict for notifications.

    Max size: CACHE_SIZE?
    msg_id is assumed to be unique.

    Use cases:
    *  Adding new Notification, always to a new unique messageid (safe)
    *  Deleting Notification after receiving a positive or negative validation
       *  Positive: retrive Notification and delete from dict (not safe!)
       *  Negative: delete from dict (safe)
    """

    def __init__(self, size: int):
        r"""Setup for notification dict.

        \param size max size of dict
        """
        super(Notifications, self).__init__()
        self.lock = threading.Lock()
        self.size = size

    def __setitem__(self, key, value: Notification):
        r"""Set or add new (key: value) pair.

        \param  key     dictionary key
        \param  value   value for key specified location
        """
        self.lock.acquire()
        try:
            if len(self) >= self.size:
                super(Notifications, self).popitem(last=False)
            return super(Notifications, self).__setitem__(key, value)
        finally:
            self.lock.release()

    def pop(self, key) -> Notification:
        r"""Get an item through its key.

        \param  key     dictionary key
        \return Notification object
        """
        self.lock.acquire()
        try:
            return super(Notifications, self).pop(key)
        finally:
            self.lock.release()

    def remove_outdated(self):
        """Remove outdated notifications."""
        try:
            self.lock.acquire()
            for msg_id, notif in list(self.items()):
                curr_time = datetime.now()  # get individual time for each
                if notif.is_timed_out(curr_time):
                    super(Notifications, self).__delitem__(msg_id)
        finally:
            self.lock.release()
