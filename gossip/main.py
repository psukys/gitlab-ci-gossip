r"""Main logic for gossip.

\package gossip
"""

import threading
import queue
import time

import listeners
import outputters
from Worker import Worker
from gossip.util.Subscriptions import Subscriptions
from gossip.util.notifications import Notifications


if __name__ == "__main__":
    # parse config
    config = conf.GossipConfig("config/sample.conf")

    work_queue = queue.Queue()
    api_queue = queue.Queue()
    p2p_queue = queue.Queue()
    subscriptions = Subscriptions()
    notifications = Notifications(config.cache_size)

    api_socket = listeners.util.create_socket(config.api_port, config.api_addr)
    api_listener = listeners.api.APIListener(sock=api_socket, queue=work_queue)

    api_outputter = outputters.Outputter.Outputter(queue=api_queue, name='API')
    p2p_outputter = outputters.Outputter.Outputter(queue=p2p_queue, name='P2P')

    # spawn 1 P2P listener thread:
    # if connection: spawn thread and give it a new socket
    # new thread listens and parses announces, peer lists?, protobuffer3

    # spawn specific amount of threads for working on work_queue
    num_workers = config.num_workers
    workers = []
    for _ in range(num_workers):
        worker = Worker(work_queue=work_queue,
                        api_queue=api_queue,
                        p2p_queue=p2p_queue,
                        subscriptions=subscriptions
                        notifications=notifications)
        workers.append(worker)
        worker.start()
