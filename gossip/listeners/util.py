r"""\brief Utility functions used in the context of listeners.

\package gossip.listeners
"""
import socket


# socket.gethostname() set this up as default?
def create_socket(port: int, addr: str) -> socket.socket:
    r"""\brief Set up the socket with given HOST and PORT.

    \param  port    specific port used in HOST
    \param  addr    specific addr to the HOST

    \return set up socket object
    """
    sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    sock.bind((addr, port))
    sock.listen(5)
    return sock
