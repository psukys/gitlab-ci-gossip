"""Universal worker threads rule the grounds here."""
import threading
import queue
import socket
import logging

from gossip import listeners  # used for identifying data in queue
from gossip.handlers.api.APIHandlerWrapper import APIHandlerWrapper
from gossip.util.Subscriptions import Subscriptions
from gossip.util.notifications import Notifications


class Worker(threading.Thread):
    """Main class for worker thread logic."""

    def __init__(self, work_queue: queue.Queue,
                 api_output: queue.Queue,
                 p2p_output: queue.Queue,
                 subscriptions: Subscriptions,
                 notifications: Notifications):
        r"""Bootstrap the worker.

        \param  queue   work queue that is shared between the producer
                        (api/p2p listeners) and consumer (worker threads)
        """
        threading.Thread.__init__(self)
        self.work_queue = work_queue
        self.params = {'api_output': api_output,
                       'p2p_output': p2p_output,
                       'subscriptions': subscriptions,
                       'notifications': notifications}
        self.finish = False
        self.logger = logging.getLogger(self.__class__.__name__)

    def run(self):
        r"""Run the thread - wait for items in queue to process them.

        Makes distinction whether it's a API or P2P call and processes it.
        """
        api_wrapper = APIHandlerWrapper(self.params)
        while not self.finish:
            queue_item = self.work_queue.get()
            conn = queue_item['conn']
            class_type = queue_item['type']
            data = self.read_socket_data(sock=conn)
            if class_type is listeners.api.APIListener:
                # for now, dismiss the result from handle_message
                self.params['socket'] = conn
                api_wrapper.handle_message(data)
            else:
                self.logger.error('Unknown class_type: {0}'.format(class_type))

    def read_socket_data(self, sock: socket.socket, block: int=1024) -> bytes:
        r"""Read all the data that socket intends to send us.

        \param  sock    socket object that is ready to transmit
        \param  block   size of a block that will be read from socket
        \return         data in bytes
        """
        data = ''

        try:
            while True:
                segment = sock.recv(block)
                data += segment
                if len(segment) < block:
                    break
        except OSError as err:
            self.logger.error('OS Error: {0}'.format(err))

        self.logger.debug('Read data (size {0}):\n{1}'.format(len(data), data))
        return data
