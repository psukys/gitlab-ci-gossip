r"""Generic outputter.

\package gossip.outputters
"""
import logging
import threading
import socket
from queue import Queue


class Outputter(threading.Thread):
    """Message outputter thread class."""

    def __init__(self, queue: Queue, name: str):
        r"""Initialize outputter.

        \param  queue   queued message for api outputter
        """
        threading.Thread.__init__(self)
        self.queue = queue
        self.logger = logging.Logger(self.__class__.__name__ +
                                     '({0})'.format(name))
        self.finish = False

    def run(self):
        r"""Listen to queue and send messages to sockets."""
        while not self.finish:
            item = self.queue.get()
            receivers = item['sockets']
            data = item['data']

            self.logger.info(
                'Sending data pack (size {0}B) to {1} peer'.format(
                    len(data), len(receivers)))
            for receiver in receivers:
                try:
                    receiver.sendall(data)
                except Exception as err:
                    # documentation does not specify the error
                    self.logger.error('Failed to send data:\n{0}'.format(err))
