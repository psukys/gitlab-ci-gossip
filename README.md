Gossip module implementation
----------------------------
## Status
[![build status](https://gitlab.com/psukys-tum/p2p-33-gossip/badges/master/build.svg)](https://gitlab.com/psukys-tum/p2p-33-gossip/commits/master) [![coverage report](https://gitlab.com/psukys-tum/p2p-33-gossip/badges/master/coverage.svg)](https://gitlab.com/psukys-tum/p2p-33-gossip/commits/master)

## Documentation
**Online documentation is available at [psukys-tum.gitlab.io/p2p-33-gossip](https://psukys-tum.gitlab.io/p2p-33-gossip)**

Documentation is generated with the use of Doxygen tool.
```
doxygen Doxyfile
```



## License
GNU GPLv3
see [LICENSE](LICENSE) for full text.
